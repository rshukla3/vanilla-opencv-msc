# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/rshukla3/vanilla-opencv-msc/Image_PreProcessing.cpp" "/Users/rshukla3/vanilla-opencv-msc/CMakeFiles/Caltech.dir/Image_PreProcessing.cpp.o"
  "/Users/rshukla3/vanilla-opencv-msc/Learn_Image.cpp" "/Users/rshukla3/vanilla-opencv-msc/CMakeFiles/Caltech.dir/Learn_Image.cpp.o"
  "/Users/rshukla3/vanilla-opencv-msc/MSC.cpp" "/Users/rshukla3/vanilla-opencv-msc/CMakeFiles/Caltech.dir/MSC.cpp.o"
  "/Users/rshukla3/vanilla-opencv-msc/Test_Generate.cpp" "/Users/rshukla3/vanilla-opencv-msc/CMakeFiles/Caltech.dir/Test_Generate.cpp.o"
  "/Users/rshukla3/vanilla-opencv-msc/main.cpp" "/Users/rshukla3/vanilla-opencv-msc/CMakeFiles/Caltech.dir/main.cpp.o"
  "/Users/rshukla3/vanilla-opencv-msc/region_of_interest.cpp" "/Users/rshukla3/vanilla-opencv-msc/CMakeFiles/Caltech.dir/region_of_interest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
